﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoesOnContainers.Web.WebMvc.Models
{
    public class PaginatedModel<TEntity>
    {
        public long Count { get; private set; }
        public int PageSize { get; private set; }
        public int PageIndex { get; private set; }
        public IEnumerable<TEntity> Data { get; private set; }

        public PaginatedModel(
                long Count,
                int PageSize,
                int PageIndex,
                IEnumerable<TEntity> Data
            )
        {
            this.Count = Count;
            this.Data = Data;
            this.PageIndex = PageIndex;
            this.PageSize = PageSize;
        }
    }
}
