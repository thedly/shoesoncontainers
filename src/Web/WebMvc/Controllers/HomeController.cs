﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ShoesOnContainers.Web.WebMvc.Services;
using ShoesOnContainers.Web.WebMvc.ViewModels;
using WebMvc.Models;

namespace WebMvc.Controllers
{
    public class HomeController : Controller
    {
        private ICatalogBrandService _catalogBrandService;
        private ICatalogTypeService _catalogTypeService;
        private IMapper _mapper;
        private ICatalogService _catalogService;
        public HomeController(
            ICatalogBrandService catalogBrandService,
            ICatalogService catalogService,
            ICatalogTypeService catalogTypeService,
            IMapper mapper
            )
        {
            _catalogBrandService = catalogBrandService;
            _catalogService = catalogService;
            _catalogTypeService = catalogTypeService;
            _mapper = mapper;
        }
        public async Task<IActionResult> Index()
        {

            var catalogs = await _catalogService.GetAllCatalogItems(10, 0);
            var brands = await _catalogBrandService.getAllBrands();
            var types = await _catalogTypeService.GetAllCatalogTypes();
            var vm = new HomeViewModel
            {
                Catalogs = catalogs.Data,
                Brands = brands,
                Types = types
            };

            return View(vm);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
