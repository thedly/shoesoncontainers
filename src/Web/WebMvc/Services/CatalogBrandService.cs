﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ShoesOnContainers.Web.WebMvc.Extensions;
using ShoesOnContainers.Web.WebMvc.Infrastructure;
using ShoesOnContainers.Web.WebMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoesOnContainers.Web.WebMvc.Services
{
    public interface ICatalogBrandService
    {
        Task<IEnumerable<CatalogBrand>> getAllBrands();
    }
    public class CatalogBrandService : ICatalogBrandService
    {
        private ICustomHttpClient _client;
        private ILogger<CatalogBrandService> _logger;
        private IOptions<AppConfiguration> _settings;
        public CatalogBrandService(
            ICustomHttpClient client, 
            ILogger<CatalogBrandService> logger,
            IOptions<AppConfiguration> settings
            )
        {
            _client = client;
            _logger = logger;
            _settings = settings;
        }


        public async Task<IEnumerable<CatalogBrand>> getAllBrands()
        {
            var uri = ApiPath.GetAllBrands(_settings.Value.BaseURI);
            var result = await _client.GetStringAsync(uri);
            return result.DeSerialize<IEnumerable<CatalogBrand>>();
        }
    }
}
