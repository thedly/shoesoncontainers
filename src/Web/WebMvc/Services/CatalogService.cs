﻿using Microsoft.Extensions.Options;
using ShoesOnContainers.Web.WebMvc.Infrastructure;
using ShoesOnContainers.Web.WebMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShoesOnContainers.Web.WebMvc.Extensions;

namespace ShoesOnContainers.Web.WebMvc.Services
{
    public interface ICatalogService
    {
        Task<PaginatedModel<CatalogItem>> GetAllCatalogItems(int pageSize, int pageIndex);
        Task<IEnumerable<CatalogItem>> GetCatalogItemFor(int id);
    }
    public class CatalogService: ICatalogService
    {
        private IOptions<AppConfiguration> _options;
        private ICustomHttpClient _client;
        public CatalogService(
                IOptions<AppConfiguration> options,
                ICustomHttpClient client
            )
        {
            _options = options;
            _client = client;
        }
        public async Task<PaginatedModel<CatalogItem>> GetAllCatalogItems(int pageSize, int pageIndex)
        {
            var uri = ApiPath.GetAllCatalogItems(_options.Value.BaseURI, pageSize, pageIndex);
            var result = await _client.GetStringAsync(uri);
            return result.DeSerialize<PaginatedModel<CatalogItem>>();
        }

        public async Task<IEnumerable<CatalogItem>> GetCatalogItemFor(int id)
        {
            var uri = ApiPath.GetCatalogItem(_options.Value.BaseURI, id);
            var result = await _client.GetStringAsync(uri);
            return result.DeSerialize<IEnumerable<CatalogItem>>();
        }
    }
}
