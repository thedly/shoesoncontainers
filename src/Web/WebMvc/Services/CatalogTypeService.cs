﻿using Microsoft.Extensions.Options;
using ShoesOnContainers.Web.WebMvc.Infrastructure;
using ShoesOnContainers.Web.WebMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShoesOnContainers.Web.WebMvc.Extensions;

namespace ShoesOnContainers.Web.WebMvc.Services
{
    public interface ICatalogTypeService
    {
        Task<IEnumerable<CatalogType>> GetAllCatalogTypes();
    }
    public class CatalogTypeService: ICatalogTypeService
    {
        private ICustomHttpClient _client;
        private IOptions<AppConfiguration> _options;
        public CatalogTypeService(
            ICustomHttpClient client,
            IOptions<AppConfiguration> options
            )
        {
            _client = client;
            _options = options;
        }
        public async Task<IEnumerable<CatalogType>> GetAllCatalogTypes() {
            var uri = ApiPath.GetAllTypes(_options.Value.BaseURI);
            var result = await _client.GetStringAsync(uri);
            return result.DeSerialize<IEnumerable<CatalogType>>();
        }
    }

    
}
