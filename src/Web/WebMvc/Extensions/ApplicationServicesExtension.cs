﻿using Microsoft.Extensions.DependencyInjection;
using ShoesOnContainers.Web.WebMvc.Infrastructure;
using ShoesOnContainers.Web.WebMvc.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoesOnContainers.Web.WebMvc.Extensions
{
    public static class ApplicationServicesExtension
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddSingleton<ICustomHttpClient, CustomHttpClient>();

            services.AddScoped<ICatalogBrandService, CatalogBrandService>();

            services.AddScoped<ICatalogService, CatalogService>();
            services.AddScoped<ICatalogTypeService, CatalogTypeService>();
        }
    }
}
