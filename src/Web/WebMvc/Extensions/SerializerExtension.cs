﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoesOnContainers.Web.WebMvc.Extensions
{
    public static class SerializerExtension
    {
        public static string Serialize<T>(this T obj)
        {
            return JsonConvert.SerializeObject(obj); 
        }

        public static T DeSerialize<T>(this string objStr)
        {
            return JsonConvert.DeserializeObject<T>(objStr);
        }
    }
}
