﻿using AutoMapper;
using ShoesOnContainers.Web.WebMvc.Models;
using ShoesOnContainers.Web.WebMvc.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoesOnContainers.Web.WebMvc
{
    public class AutoMapperConfig: Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<CatalogBrandViewModel, CatalogBrand>();
            CreateMap<CatalogBrand, CatalogBrandViewModel>();

            CreateMap<CatalogItem, CatalogViewModel>();

            
        }
    }
}
