﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoesOnContainers.Web.WebMvc.ViewModels
{
    public class CatalogBrandViewModel
    {
        public int Id { get; set; }
        public string Brand { get; set; }
    }
}
