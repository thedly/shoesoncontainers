﻿using ShoesOnContainers.Web.WebMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoesOnContainers.Web.WebMvc.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<CatalogItem> Catalogs { get; set; }
        public IEnumerable<CatalogBrand> Brands { get; set; }
        public IEnumerable<CatalogType> Types { get; set; }
    }
}
