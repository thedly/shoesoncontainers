﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoesOnContainers.Web.WebMvc.Infrastructure
{
    public class ApiPath
    {
        public static string GetCatalogItem(string baseUri, int id) {
            return $"{baseUri}/api/Catalog/items/{id}";
        }
        public static string GetAllCatalogItems(string baseUri, int pageSize, int pageIndex)
        {
            return $"{baseUri}/api/Catalog/items?pageSize={pageSize}&pageIndex={pageIndex}";
        }
        public static string GetAllBrands(string baseUri)
        {
            return $"{baseUri}/api/Catalog/Brands";
        }
        public static string GetAllTypes(string baseUri)
        {
            return $"{baseUri}/api/Catalog/Types";
        }
    }
}
