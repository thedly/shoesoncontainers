﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProductCatalogApi.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalogApi.Data
{
    public class CatalogContext: DbContext
    {
        public CatalogContext (DbContextOptions options): base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CatalogBrand>(ConfigureCatalogBrand);
            modelBuilder.Entity<CatalogType>(ConfigureCatalogType);
            modelBuilder.Entity<CatalogItem>(ConfigureCatalogItem);
        }

        private void ConfigureCatalogType(EntityTypeBuilder<CatalogType> obj)
        {
            obj.ToTable("CatalogType");
            obj.Property(g => g.Id)
                .ForSqlServerUseSequenceHiLo("catalog_type_hilo")
                .IsRequired(true);
            obj.Property(g => g.Type)
                .IsRequired(true)
                .HasMaxLength(100);
        }

        private void ConfigureCatalogItem(EntityTypeBuilder<CatalogItem> obj)
        {
            obj.ToTable("Catalog");
            obj.Property(g => g.Id)
                .ForSqlServerUseSequenceHiLo("catalog_hilo")
                .IsRequired(true);
            obj.Property(g => g.Name)
                .IsRequired(true)
                .HasMaxLength(50);
            obj.Property(g => g.Price)
                .IsRequired(true);
            obj.Property(g => g.PictureUrl)
                .IsRequired(false);
            obj.HasOne(g => g.CatalogBrand)
                .WithMany()
                .HasForeignKey(f => f.CatalogBrandId);
            obj.HasOne(g => g.CatalogType)
                .WithMany()
                .HasForeignKey(f => f.CatalogTypeId);
        }

        private void ConfigureCatalogBrand(EntityTypeBuilder<CatalogBrand> obj)
        {
            obj.ToTable("CatalogBrand");
            obj.Property(g => g.Id)
                .ForSqlServerUseSequenceHiLo("catalog_brand_hilo")
                .IsRequired(true);
            obj.Property(g => g.Brand)
                .IsRequired(true)
                .HasMaxLength(100);
        }

        public DbSet<CatalogType> CatalogTypes { get; set; }
        public DbSet<CatalogBrand> CatalogBrands { get; set; }
        public DbSet<CatalogItem> CatalogItems { get; set; }
    }
}
