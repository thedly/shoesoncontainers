﻿using ProductCatalogApi.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalogApi.Data
{
    public class CatalogSeed
    {
        public static async Task SeedAsync(CatalogContext context)
        {
            await context.Database.EnsureCreatedAsync();

            if (!context.CatalogBrands.Any()) {
                context.CatalogBrands.AddRange(GetPreconfiguredCatalogBrands());
                await context.SaveChangesAsync();
            }

            if (!context.CatalogTypes.Any())
            {
                context.CatalogTypes.AddRange(GetPreconfiguredCatalogTypes());
                await context.SaveChangesAsync();
            }

            if (!context.CatalogItems.Any())
            {
                context.CatalogItems.AddRange(GetPreconfiguredCatalogItems());
                await context.SaveChangesAsync();
            }

        }

        static IEnumerable<CatalogBrand> GetPreconfiguredCatalogBrands() {
            return new List<CatalogBrand>() {
                new CatalogBrand{ Brand = "Adidas"},
                new CatalogBrand{ Brand = "Puma" },
                new CatalogBrand{ Brand = "Slazenger" }
            };
        }

        static IEnumerable<CatalogType> GetPreconfiguredCatalogTypes()
        {
            return new List<CatalogType>() {
                new CatalogType{ Type = "Running"},
                new CatalogType{ Type = "Basketball" },
                new CatalogType{ Type = "Tennis" }
            };
        }

        static IEnumerable<CatalogItem> GetPreconfiguredCatalogItems()
        {
            return new List<CatalogItem>() {
                new CatalogItem{ CatalogBrandId = 3, CatalogTypeId = 2, Description = "Shoes for next century", Name = "World star", Price = 230.2M, PictureUrl = "http://externalcatalogbaseurltobeplaces/api/pic/1" },
                new CatalogItem{ CatalogBrandId = 1, CatalogTypeId = 1, Description = "One other description", Name = "White lane", Price = 200.2M, PictureUrl = "http://externalcatalogbaseurltobeplaces/api/pic/2" },
                new CatalogItem{ CatalogBrandId = 2, CatalogTypeId = 3, Description = "Some description", Name = "Prism white shoes", Price = 88.2M, PictureUrl = "http://externalcatalogbaseurltobeplaces/api/pic/3" },
            };
        }
    }
}
