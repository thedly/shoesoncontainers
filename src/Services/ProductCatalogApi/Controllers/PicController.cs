﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ProductCatalogApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Pic")]
    public class PicController : Controller
    {
        private IHostingEnvironment _hostingEnv;
        public PicController(IHostingEnvironment hostingEnv)
        {
            _hostingEnv = hostingEnv;
        }
        [HttpGet]
        [Route("{imageId}")]
        public async Task<IActionResult> GetImage(string imageId)
        {
            var webRoot = _hostingEnv.WebRootPath;
            var path = Path.Combine(webRoot, "Pics", imageId);
            var buffer = System.IO.File.ReadAllBytes(path);
            return File(buffer, "Image/png");
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> AddImage(IFormFile file)
        {
            var webRoot = _hostingEnv.WebRootPath;
            var fileName = file.FileName.Split(".")[0];
            var fileExt = file.FileName.Split(".")[1];
            var imageName = $"{fileName}_{Guid.NewGuid()}.{fileExt}";
            var filePath = Path.Combine(webRoot, "Pics", imageName);
            if (file.Length > 0)
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }

            return Ok(new { ImageName = imageName, imageSize = file.Length });
        }
    }
}