﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ProductCatalogApi.Data;
using ProductCatalogApi.Domain;
using ProductCatalogApi.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalogApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Catalog")]
    public class CatalogController : Controller
    {
        private readonly CatalogContext _catalogContext;
        private readonly IOptions<CatalogSettings> _catalogSettings;
        public CatalogController(
            CatalogContext catalogContext,
            IOptions<CatalogSettings> catalogSettings
            )
        {
            _catalogContext = catalogContext;
            _catalogSettings = catalogSettings;
            _catalogContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        [HttpGet]
        [Route("Types")]
        public async Task<IActionResult> CatalogTypes()
        {
            var items = await _catalogContext.CatalogTypes.ToListAsync();
            return Ok(items);
        }
        [HttpGet]
        [Route("Brands")]
        public async Task<IActionResult> CatalogBrands()
        {
            var items = await _catalogContext.CatalogBrands.ToListAsync();
            return Ok(items);
        }
        [HttpGet]
        [Route("Items/{id}")]
        public async Task<IActionResult> GetItemById(int id)
        {
            if (id <= 0)
                return BadRequest();

            var item = await _catalogContext.CatalogItems.FirstOrDefaultAsync(g => g.Id == id);
            if (item != null)
                return Ok(item);
            else
                return NotFound();
        }
        [HttpGet]
        [Route("Items")]
        public async Task<IActionResult> Items([FromQuery] int pageSize = 6, [FromQuery] int pageIndex = 0)
        {
            var totalItems = await _catalogContext.CatalogItems.LongCountAsync();
            var itemsOnPage = await _catalogContext.CatalogItems
                .OrderBy(g => g.Name)
                .Skip(pageIndex * pageSize)
                .Take(pageSize)
                .ToListAsync();

            var model = new PaginatedItemsViewModel<CatalogItem>(totalItems, pageSize, pageIndex, itemsOnPage);
            return Ok(model);
        }

        [HttpPost]
        [Route("Items/Add")]
        public async Task<IActionResult> Add([FromBody] CatalogItem item)
        {
            try
            {
                var addItem = await _catalogContext.CatalogItems
                .AddAsync(item);
                await _catalogContext.SaveChangesAsync();
                var addedItem = addItem.Entity.Id;
                return Created($"api/Catalog/Items/{addedItem}", null);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            
        }

        [HttpPost]
        [Route("Brands/Add")]
        public async Task<IActionResult> AddBrands([FromBody] CatalogBrand item)
        {
            try
            {
                var addItem = await _catalogContext.CatalogBrands
                .AddAsync(item);
                await _catalogContext.SaveChangesAsync();
                var addedItem = addItem.Entity.Id;
                return Created($"api/Catalog/Items/{addedItem}", new { Success = true, AddedItemId = addedItem, url = $"api/Catalog/Items/{addedItem}" });
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }

        }

        [HttpPost]
        [Route("Types/Add")]
        public async Task<IActionResult> AddTypes([FromBody] CatalogType item)
        {
            try
            {
                var addItem = await _catalogContext.CatalogTypes
                .AddAsync(item);
                await _catalogContext.SaveChangesAsync();
                var addedItem = addItem.Entity.Id;
                return Created($"api/Catalog/Items/{addedItem}", new { Success = true, AddedItemId = addedItem, url = $"api/Catalog/Items/{addedItem}" });
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }

        }

    }
}